<?php
/**
 * Curse Inc.
 * Staff Management
 * Staff Management Hooks
 *
 * @author		Alex Smith
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class StaffManagementHooks {
	/**
	 * Setups and Modifies Database Information
	 *
	 * @access	public
	 * @param	object $updater	[Optional] DatabaseUpdater Object
	 * @return	boolean	true
	 */
	public static function onLoadExtensionSchemaUpdates(DatabaseUpdater $updater) {
		$extDir = __DIR__;

		$updater->addExtensionUpdate(array('addTable', 'report_card_thresholds', "{$extDir}/install/sql/staffmanagement_table_report_card_thresholds.sql", true));
		$updater->addExtensionUpdate(array('addTable', 'report_card_metadata', "{$extDir}/install/sql/staffmanagement_table_report_card_metadata.sql", true));
		$updater->addExtensionUpdate(array('addTable', 'report_card_users', "{$extDir}/install/sql/staffmanagement_table_report_card_users.sql", true));

		return true;
	}

	/**
	 * Adds the report as an attachment to ReportCard emails
	 *
	 * @param string $to array of MailAdresses of the targets
	 * @param string $from MailAddress of the sender
	 * @param string &$subject email subject (not MIME encoded)
	 * @param array &$headers email headers (except To: and Subject:) as an array of header name
	 *                         => value pairs
	 * @param string &$body email body (in MIME format) as a string
	 * @param string &$error should be set to an error message string
	 */
	public static function onUserMailerTransformMessage( $to, $from, &$subject, &$headers, &$body,
														 &$error ) {
		// Replace the email body to add our attachment when ReportCard has set this header
		if ( array_key_exists( 'X-Report-Card-ID', $headers ) ) {
			$reportID = $headers['X-Report-Card-ID'];
			ReportCard::insertEmailAttachment( $reportID, $headers, $body );
		}

		return true;
	}
}
