<?php
/**
 * Curse Inc.
 * Staff Management
 * User Stats Special Page
 *
 * @author		Collin Klopfenstein
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class SpecialUserStats extends HydraCore\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var		string
	 */
	private $content;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct('UserStats', 'staffmanagement', false);
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->templateUserStats = new TemplateUserStats;

		$this->output->addModuleStyles('ext.staffManagement.styles');
		$this->output->addModules('ext.staffManagement.scripts');

		$this->setHeaders();

		$this->userWikiStatsPage($this->wgRequest->getInt('globalId'));

		$this->output->addHTML($this->content);
	}
	
	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function userWikiStatsPage($globalId) {
		if ($globalId < 1) {
			$listPage = Title::newFromText('Special:GlobalStats');
			$listPage = $this->getTitleFor('Special:GlobalStats');
			$this->output->redirect($listPage->getFullURL());
			return;
		}

		$itemsPerPage = 100;
		$start = $this->getRequest()->getInt('st', 0);

		$lookup = \CentralIdLookup::factory();
		$user = $lookup->localUserFromCentralId($globalId);

		if (!$user || !$user->getId()) {
			$this->output->showErrorPage('staff_management_error', 'error_user_not_found_in_database');
			return;
		}

		$userWikiStats = [
			'user'			=> $user,
			'wikis'			=> []
		];

		foreach (range(0, 2) as $monthsAgo) {
			$months[] = date('M Y', strtotime('first day of '.$monthsAgo.' month ago'));
		}

		$stats = StaffManagement::$trackedStats;
		foreach ($stats as $stat) {
			$filters = [
				'user_id' 			=> $globalId,
				'stat'				=> $stat,
				'limit'				=> $itemsPerPage,
				'offset'			=> $start,
				'sort_direction'	=> 'desc'
			];

			$counts = \Cheevos\Cheevos::getStatMonthlyCount($filters);
			if (is_array($counts)) {
				foreach ($counts as $count) {
					$date = date('M Y', $count->getMonth());
					if (!isset($userWikiStats['wikis'][$count->getSite_Key()])) {
						foreach ($months as $month) {
							$userWikiStats['wikis'][$count->getSite_Key()][$stat][$month] = '0';
						}
					}
					$userWikiStats['wikis'][$count->getSite_Key()][$count->getStat()][$date] = $count->getCount();
				}
			}
		}
		foreach ($userWikiStats['wikis'] as $siteKey => $stat) {
			foreach ($stats as $stat) {
				if (!isset($userWikiStats['wikis'][$siteKey][$stat])) {
					foreach ($months as $month) {
						$userWikiStats['wikis'][$siteKey][$stat][$month] = '0';
					}
				}
				$userWikiStats['wikis'][$siteKey][$stat]['total'] = 0;
				foreach ($months as $month) {
					if (isset($userWikiStats['wikis'][$siteKey][$stat][$month])) {
						$userWikiStats['wikis'][$siteKey][$stat]['total'] += $userWikiStats['wikis'][$siteKey][$stat][$month];
					}
				}
			}
		}

		$sortField = $this->wgRequest->getVal('field') ? $this->wgRequest->getVal('field') : 'wiki_points';
		$sortTime = $this->wgRequest->getVal('time') ? $this->wgRequest->getVal('time') : 'total';

		$sort = [];
		foreach ($userWikiStats['wikis'] as $siteKey => $wiki) {
			$site = \DynamicSettings\Wiki::loadFromHash($siteKey);
			if ($site !== false) {
				$userWikiStats['wikis'][$siteKey]['site'] = $site;
			} else {
				unset($userWikiStats['wikis'][$siteKey]);
				continue;
			}
			$sort[$siteKey] = $wiki[$sortField][$sortTime];
		}
		
		if ($this->wgRequest->getVal('dir') == 'asc') {
			asort($sort);
			$sortDir = 'asc';
		} else {
			arsort($sort);
			$sortDir = 'desc';
		}
		
		$this->output->setPageTitle(wfMessage('userstats')->escaped().' - '.$userWikiStats['user']->getName());
		$this->content = $this->templateUserStats->userWikiStatsPage($userWikiStats, $stats, $months, $sort, $sortDir, $sortField, $sortTime);
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access	protected
	 * @return	string
	 */
	protected function getGroupName() {
		return 'staffmanagement';
	}
}
