<?php
/**
 * Curse Inc.
 * Staff Management
 * Wiki Stats Special Page
 *
 * @author		Collin Klopfenstein, Tim Aldridge
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class SpecialWikiStats extends HydraCore\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var		string
	 */
	private $content;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct('WikiStats', 'staffmanagement');
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->templateWikiStats = new TemplateWikiStats;

		$this->output->addModuleStyles('ext.staffManagement.styles');
		$this->output->addModules('ext.staffManagement.scripts');

		$this->setHeaders();

		$this->wikiStats();

		$this->output->addHTML($this->content);
	}
	
	/**
	 * Wiki Stats List
	 *
	 * @access	public
	 * @return	void
	 */
	public function wikiStats() {
		$start			= $this->wgRequest->getInt('st');
		$itemsPerPage	= 25;
		$searchTerm		= null;
		$cookieExpire	= time() + 900;

		if ($this->wgRequest->getVal('do') == 'resetSearch') {
			$this->wgRequest->response()->setcookie('wikiStatsSearchTerm', '', 1);
		} else {
			$listSearch = $this->wgRequest->getVal('list_search');
			$cookieSearch = $this->wgRequest->getCookie('wikiStatsSearchTerm');
			if (($this->wgRequest->getVal('do') == 'search' && !empty($listSearch)) || !empty($cookieSearch)) {
				if (!empty($cookieSearch) && empty($listSearch)) {
					$searchTerm = $cookieSearch;
				} else {
					$searchTerm = $listSearch;
				}
				$this->wgRequest->response()->setcookie('wikiStatsSearchTerm', $searchTerm, $cookieExpire);
			}
		}

		if ($this->wgRequest->getCookie('wikiStatsSortKey') && !$this->wgRequest->getVal('sort')) {
			$sort = $this->wgRequest->getCookie('wikiStatsSortKey');
		} else {
			$sort = $this->wgRequest->getVal('sort');
		}
		if (!empty($sort)) {
			$sortKey = $sort;
		} else {
			$sortKey = 'wiki_name';
		}
		$this->wgRequest->response()->setcookie('wikiStatsSortKey', $sortKey, $cookieExpire);

		$sortDir = $this->wgRequest->getVal('sort_dir');
		if (($this->wgRequest->getCookie('wikiSitesSortDir') == 'desc' && !$sortDir) || strtolower($sortDir) == 'desc') {
			$sortDir = 'DESC';
		} else {
			$sortDir = 'ASC';
		}
		$this->wgRequest->response()->setcookie('wikiSitesSortDir', $sortDir, $cookieExpire);

		$wikis = \DynamicSettings\Wiki::loadFromSearch($start, $itemsPerPage, $searchTerm, $sortKey, $sortDir, true);

		$total = \DynamicSettings\Sites::getTotalSites($searchTerm, true, false);

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total, $itemsPerPage, $start);

		$this->output->setPageTitle(wfMessage('wikistats')->escaped());
		$this->content = $this->templateWikiStats->wikiStats($wikis, $pagination, $sortKey, strtolower($sortDir), $searchTerm);
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access	protected
	 * @return	string
	 */
	protected function getGroupName() {
		return 'staffmanagement';
	}
}
