<?php
/**
 * Curse Inc.
 * Staff Management
 * Report Card Special Page
 *
 * @author		Alex Smith
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class SpecialReportCard extends HydraCore\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var		string
	 */
	private $content;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct('ReportCard', 'view_report_cards');
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();
		$this->templateReportCard = new TemplateReportCard;
		$this->output->addModuleStyles('ext.staffManagement.styles');
		$this->output->addModules('ext.staffManagement.scripts');
		$this->setHeaders();

		switch( strtolower($subpage) ) {
			case "thresholds":
				$this->thresholdsForm();
			break;
			case "email":
				$this->reportEmail();
			break;
			default:
				$this->reportCardForm();
			break;
		}
	}

	/**
	 * Report Card
	 *
	 * @access	public
	 * @return	void	[Outputs to Screen]
	 */
	public function reportCardForm() {
		$this->output->setPageTitle(wfMessage('report_card')->escaped());
		$this->content = $this->templateReportCard->reportCardForm();
		$this->output->addHTML($this->content);
	}

	/**
	 * [sendReportEmailPage description]
	 * @return void	[Outputs to Screen]
	 */
	public function reportEmail() {
		$this->output->setPageTitle(wfMessage('report_card')->escaped());

		$rc = new \ReportCard();
		$success = $rc->sendReportEmail( [ $this->wgUser ] );

		if ($success) {
			$this->output->addHTML("Report email has been sent to you.");
		} else {
			$this->output->addHTML("Report email failed. Sorry.");
		}

	}

	/**
	 * Thresholds Form
	 *
	 * @access	public
	 * @return	void	[Outputs to Screen]
	 */
	public function thresholdsForm() {
		$thresholds = StaffManagement::getThresholds();

		if ($this->wgRequest->getVal('do') == 'save') {
			$thresholdPoints = $this->wgRequest->getArray('thresholdPoints');
			$thresholdScores = $this->wgRequest->getArray('thresholdScores');
			if (count($thresholdPoints) == count($thresholdScores)) {
				$total = count($thresholdPoints);
				$thresholds = array();
				for ($i=0; $i < $total; $i++) {
					$thresholds[intval($thresholdPoints[$i])] = $thresholdScores[$i];
				}
			}

			if (!count($errors)) {
				$success = StaffManagement::saveThresholds($thresholds);
			}
		}

		$this->output->setPageTitle(wfMessage('report_card_thresholds')->escaped());
		$this->content = $this->templateReportCard->thresholdsForm($thresholds, $errors, $success);
		$this->output->addHTML($this->content);
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access	protected
	 * @return	string
	 */
	protected function getGroupName() {
		return 'staffmanagement';
	}
}
