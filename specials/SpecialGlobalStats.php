<?php
/**
 * Curse Inc.
 * Staff Management
 * Global Stats Special Page
 *
 * @author		Collin Klopfenstein
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class SpecialGlobalStats extends HydraCore\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var		string
	 */
	private $content;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct('GlobalStats', 'staffmanagement');
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->templateGlobalStats = new TemplateGlobalStats;

		$this->output->addModuleStyles('ext.staffManagement.styles');
		$this->output->addModules('ext.staffManagement.scripts');

		$this->setHeaders();

		$this->globalStats();

		$this->output->addHTML($this->content);
	}

	/**
	 * User List
	 *
	 * @access	public
	 * @return	void
	 */
	public function globalStats() {
		$start			= ($this->wgRequest->getInt('st') ? $this->wgRequest->getInt('st') : 0);
		$itemsPerPage	= 25;
		$searchTerm = '';

		if ($this->wgRequest->getVal('do') == 'resetSearch') {
			$this->wgRequest->response()->setcookie('globalStatsSearchTerm', '', 1);
		} else {
			$listSearch = $this->wgRequest->getVal('list_search');
			$cookieSearch = $this->wgRequest->getCookie('globalStatsSearchTerm');
			if (($this->wgRequest->getVal('do') == 'search' && !empty($listSearch)) || !empty($cookieSearch)) {
				if (!empty($cookieSearch) && empty($listSearch)) {
					$searchTerm = $cookieSearch;
				} else {
					$searchTerm = $listSearch;
				}
				$this->wgRequest->response()->setcookie('globalStatsSearchTerm', $searchTerm, $cookieExpire);
			}
		}

		$sortDir = (strtolower($this->wgRequest->getVal('dir')) == 'asc' ? 'asc' : 'desc');
		$sortField = (in_array($this->wgRequest->getVal('field'), StaffManagement::$trackedStats) ? $this->wgRequest->getVal('field') : 'wiki_points');

		$globalIds = [];
		$lookup = \CentralIdLookup::factory();
		if (!empty($searchTerm)) {
			$user = User::newFromName($searchTerm);
			$globalId = $lookup->centralIdFromLocalUser($user);
			if ($user instanceOf User && $user->getId() && $globalId) {
				$globalIds[] = $globalId;
			}
		}

		$statProgress = [];
		foreach (StaffManagement::$trackedStats as $stat) {
			$filters = [
				'stat'		=> $stat,
				'global'	=> true
			];
			if ($stat === $sortField) {
				$filters['sort_direction'] = $sortDir;
			}

			foreach (range(0, 3) as $monthsAgo) {
				if ($monthsAgo > 0) {
					$filters['start_time'] = strtotime(date('Y-m-d', strtotime('first day of '.$monthsAgo.' month ago')).'T00:00:00+00:00');
					$filters['end_time'] = strtotime(date('Y-m-d', strtotime('last day of last month')).'T23:59:59+00:00');
				}

				try {
					$_statProgress = \Cheevos\Cheevos::getStatProgress($filters);
					foreach ($_statProgress as $_progress) {
						$statProgress[$_progress->getUser_Id()][$monthsAgo][$stat] = $_progress;
					}
				} catch (\Cheevos\CheevosException $e) {
					throw new \MWException("Encountered Cheevos API error {$e->getMessage()}\n");
				}
			}
		}

		if (count($statProgress)) {
			foreach ($statProgress as $globalId => $progresses) {
				if ($globalId < 1) {
					unset($statProgress[$globalId]);
					continue;
				}
				$user = $lookup->localUserFromCentralId($globalId);
				if (!$user) {
					unset($statProgress[$globalId]);
					continue;
				}
				$statProgress[$globalId]['user'] = $user;
			}
		}

		$total = 0;

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $total['total'], $itemsPerPage, $start);

		$this->output->setPageTitle(wfMessage('globalstats')->escaped());
		$this->content = $this->templateGlobalStats->globalStats($statProgress, $pagination, $sortDir, $sortField, $searchTerm);
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access	protected
	 * @return	string
	 */
	protected function getGroupName() {
		return 'staffmanagement';
	}
}
