$(document).ready(function() {
	$('.add_threshold').click(function() {
		var fieldset = $(this).parent().parent();
		var delImg = $('<img>').attr('src', mw.config.get('wgScriptPath')+'/extensions/StaffManagement/images/delete.png').addClass('delete_threshold');
		var thresholdPoints = $('<input>').attr('name', 'thresholdPoints[]').attr('type', 'text');
		var thresholdScores = $('<input>').attr('name', 'thresholdScores[]').attr('type', 'text');
		var div = $('<div>').addClass('threshold').append(thresholdPoints).append(thresholdScores).append(delImg);
		fieldset.append(div);
	});
	function setupDeleteValue() {
		$('.delete_threshold').click(function() {
			$(this).parent().remove();
		});
	}
	setupDeleteValue();

	var userControls = '<div class="controls_container">'
		+ '<span class="fa fa-wrench"></span>'
		+ '<span class="dropdown">'
		+ '<a href="#" class="clearMeta"><span class="fa fa-remove"></span> Clear Meta Data</a>'
		+ '<a href="#" class="removeUser"><span class="fa fa-remove"></span> Remove User</a>'
		+ '</span>'
		+ '</div>';

	var columns = [];

	$("#reportcard th").each(function(){
		columns.push({"data":$(this).attr('data-id'),"class":$(this).attr('data-id')});
		$(this).attr('data-column',columns.length-1);
	});

	$.fn.dataTable.ext.errMode = 'none';
	
	var table = $("#reportcard")
	.on( 'error.dt', function (e,settings,techNote,message){
        console.log( 'An error has been reported by DataTables: ', e,settings,techNote,message );
	}).on( 'xhr.dt', function ( e, settings, json, xhr){
        console.log( 'DataTables xhr: ', e,settings,json, xhr );
	}).DataTable({
		scrollX :true,
		paging: false,
		ajax: {
			url: '/api.php?action=reportcardapi&format=json&x=api',
			dataSrc: 'data'
		},
		columns: columns,
		columnDefs: [ {
			targets: -1,
			data: null,
			defaultContent: userControls,
			className: "dt-body-center controls"
		} ],
		//responsive: true,
		buttons: [
			{
				extend: 'collection',
				text: 'Export',
				buttons: [
					'excel',
					'csv',
					'print',
					{
						text: 'Email Me CSV',
						action: function(){
							window.location.href = "/Special:ReportCard/email";
						}
					},
				]
			},
			'colvis',
			{
				text: 'Show Last 90',
				action: toggleVisibleAction
			},
			{
				text: 'Add User',
				action: addUserAction
			},
			{
				text: 'Refresh Table',
				action: function() {
					table.ajax.reload();
				}
			}
			/*{
				text: 'Manage Thresholds',
				action: function(){
					window.location.href = "/Special:ReportCard/thresholds";
				}
			}*/
		],
		dom: "Bfrtip",
		rowCallback: function( row, data, index ) {
			console.log(data);

			if (!data.global_id) {
				$(row).addClass('row_error');
				$(row).find('td.notes').append('<div class="note_error"><br />Error: No Global ID was found for this user.</div>')
			}
		
			// append userid to any links. For science.
			$(row).find('a').each(function(){
				$(this).attr('data-userid',data.user_id);
			});
			/**
			 * For future user of cell highlighting...
				if ( data[4] == "A" ) {
				$('td:eq(4)', row).html( '<b>A</b>' );
				}
			*/
		}
	});

	table.on('draw', function() {
		var tableContent = $(table.table().body());
		var keyword = table.search();
		tableContent.unmark();
		tableContent.mark(keyword, {
		"separateWordSearch": true
		});
	});

	function groupVisibility(group,visible) {
		table.columns().every( function () {
			var thisGroup = $(this.header()).attr('data-group')
			if (thisGroup == group){
				this.visible(visible)
			}
		});
	}

	function toggleVisibleAction( e, dt, node, config ) {
		console.log(e,dt,node,config);
		if (config.text == "Show Last 90") {
			groupVisibility('90',true);
			groupVisibility('30',false);
			$(e.toElement).html('Show Last 30');
			config.text = "Show Last 30";
		} else {
			groupVisibility('30',true);
			groupVisibility('90',false);
			$(e.toElement).html('Show Last 90');
			config.text = "Show Last 90";
		}
	}

	$('#reportcard tbody').on( 'click', '.clearMeta', function (e) {
		e.preventDefault();
		var user_id = $(this).attr('data-userid')
		if(confirm('Are you sure you want to clear this users meta data?')) {
			$.post( "/api.php?action=reportcardapi&format=json&x=api/deletemeta", {
				user_id: user_id,
			}, function( data ) {
			 	table.ajax.reload(); // get new calculations from backend. Im lazy.
			});
		}
	});

	$('#reportcard tbody').on( 'click', '.removeUser', function (e) {
		e.preventDefault();
		var user_id = $(this).attr('data-userid')
		if(confirm('Are you sure you want to remove this user?')) {
			$.post( "/api.php?action=reportcardapi&format=json&x=api/deleteuser", {
				user_id: user_id,
			}, function( data ) {
			 	table.ajax.reload(); // get new calculations from backend. Im lazy.
			});
		}
	});

	$('#reportcard tbody').on( 'dblclick', 'td', function () {
		var cell = table.cell( this );
		if (typeof cell.index() == undefined) {
			return;
		}
		var header = $( table.column( cell.index().column ).header() );
		var editable = header.attr('data-editable')
		if (editable == "true") {
			var editing = $(this).attr("data-editing");
			var rowData = table.row(cell.index().row).data();
			var field = header.attr('data-id');
			if (editing == "false" || typeof editing == "undefined") {
				$(this).attr("data-editing","true").addClass('editing-highlight').removeClass( 'editable-highlight' );
				cell.data('<input type="text" class="editing_cell" data-userid="'+rowData.user_id+'" data-field="'+field+'" value="'+cell.data()+'" />');
				$(this).find('input').focus();
			} else {
				$(this).attr("data-editing","false").removeClass('editing-highlight');
				var value = $(this).find('.editing_cell').val();
				cell.data(value);
				apiSaveValue(rowData.user_id,field,value);
			}
		}
	} );

	// Highlight editable cells on hover.
	$('#reportcard tbody').on( 'mouseenter', 'td', function () {
		var cell = table.cell( this );
		if (cell) {
			var header = $( table.column( cell.index().column ).header() );
			var editable = header.attr('data-editable');
			if(editable == "true" && !$(this).hasClass('editing-highlight')) {
				$( this ).addClass( 'editable-highlight' );
			}
		}
	} ).on( 'mouseleave', 'td', function(){
			$( table.cells().nodes() ).removeClass( 'editable-highlight' );
	});
		
	$("#reportcard").on('blur','input',function (e) {
			$(this).attr("data-editing","false").removeClass('editing-highlight');
			var value = $(this).val();
			var user_id = $(this).attr('data-userid');
			var field = $(this).attr('data-field');
			table.cell( $(this).parent() ).data(value);
			apiSaveValue(user_id,field,value);

	});

	$("#reportcard").on('keyup','input',function (e) {
		 if (e.keyCode == 13) {
			$(this).blur();
		}
	});

	function apiSaveValue(user_id,field,value) {
		if (typeof value !== "undefined") {
			$.post( "/api.php?action=reportcardapi&format=json&x=api/save", {
				user_id: user_id,
				field: field,
				value: value
			}, function( data ) {
			 	table.ajax.reload(); // get new calculations from backend. Im lazy.
			});
		}
	}

	function addUserAction() {
		var user_name = prompt("What user would you like to add?")
		$.post( "/api.php?action=reportcardapi&format=json&x=api/savenew", {
			user_name: user_name,
		}, function( data ) {
			console.log(data);
			if (typeof data.data !== 'undefined') {
				data = data.data;
			}
			if (data.error) {
				alert(data.error);
			} else if (data.warnings) {
				alert(data.warnings);
			} else {
				if (data.message) {
					console.log(data.message);
				}
				table.ajax.reload();
			}
		});
	}

	// hide 90 day by default.
	groupVisibility('hide',false);
	groupVisibility('90',false);
	groupVisibility('30',true);
});