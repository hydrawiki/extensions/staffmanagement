<?php
/**
 * Curse Inc.
 * Staff Management
 * Staff Management Class
 *
 * @author		Collin Klopfenstein
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class StaffManagement {
	/**
	 * Mediawiki Database Object
	 *
	 * @var		object
	 */
	static private $DB;

	/**
	 * Threshold Values
	 *
	 * @var		array
	 */
	static private $thresholds;

	/**
	 * Class Initialized
	 *
	 * @var		boolean
	 */
	static private $initialized = false;

	/**
	 * Tracked Stats
	 *
	 * @var		array
	 */
	static public $trackedStats = [
		'wiki_points',
		'article_edit',
		'article_delete',
		'admin_patrol',
		'admin_block_ip'
	];

	/**
	 * Main Initializer
	 *
	 * @access	public
	 * @return	void
	 */
	public static function init() {
		if (!self::$initialized) {
			self::$DB = wfGetDB(DB_MASTER);

			self::$initialized = true;
		}
	}

    /**
     * Main Initializer
     *
     * @access	public
     * @return	void
     */
    public static function initWithThresholds() {
        if (!self::$initialized) {
            self::$DB = wfGetDB(DB_MASTER);

            self::loadThresholds();

            self::$initialized = true;
        }
    }

	/**
	 * Get score based on a threshold.
	 *
	 * @access	public
	 * @param	integer	The number of actions or points for this calculation.
	 * @param	integer	[Optional] The number of days this score is for such as 30 days, 60 days, or 90 days.
	 * @return	mixed	Score rating as a string or null for no score.
	 */
	static public function getScore($points, $timeSpan = null) {
		self::initWithThresholds();

		if (is_array(self::$thresholds)) {
			foreach (self::$thresholds as $thresholdPoints => $thresholdScore) {
				if ($timeSpan > 0) {
					$thresholdPoints = $thresholdPoints * $timeSpan;
				}
				if ($points >= $thresholdPoints) {
					return $thresholdScore;
				}
			}
		}

		return null;
	}

	/**
	 * Load the threshold values from the database.
	 *
	 * @access	public
	 * @return	array	Array of thresholds containing $point => $score key value pairs.
	 */
	static public function getThresholds() {
		self::initWithThresholds();

		return self::$thresholds;
	}

	/**
	 * Load the threshold values from the database.
	 *
	 * @access	private
	 * @return	void
	 */
	static private function loadThresholds() {
		$result = self::$DB->select(
			'report_card_thresholds',
			['*'],
			[],
			__METHOD__
		);

		while ($row = $result->fetchRow()) {
			self::$thresholds[$row['points']] = $row['score'];
		}
		@asort(self::$thresholds);
	}

	/**
	 * Load the threshold values from the database.
	 *
	 * @access	public
	 * @param	array	Array of thresholds containing $point => $score key value pairs.
	 * @return	boolean	Successful Save
	 */
	static public function saveThresholds($thresholds) {
		global $wgDBprefix;
		self::initWithThresholds();

		try {
			self::$DB->startAtomic(__METHOD__);
			self::$DB->query("TRUNCATE TABLE {$wgDBprefix}report_card_thresholds;");

			foreach ($thresholds as $points => $score) {
				self::$DB->insert('report_card_thresholds', array('points' => $points, 'score' => $score), __METHOD__);
			}
			self::$DB->endAtomic(__METHOD__);
		} catch (Exception $e) {
			self::$DB->cancelAtomic(__METHOD__);
			return false;
		}
		return true;
	}
}
?>