<?php
/**
 * Curse Inc.
 * StaffManagement
 * Report Card Data Cache Job
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		StaffManagement
 * @link		https://gitlab.com/hydrawiki
 *
**/

namespace StaffManagement\Job;

class ReportCardDataCacheJob extends \SyncService\Job {
	/**
	 * Runs the report card data generation.
	 *
	 * @access	public
	 * @param	array $args	Unused
	 * @return	integer	Exit value for this thread.
	 */
	public function execute($args = []) {
		try {
			$rc = new \ReportCard();
			$rc->getReportData(true);
		} catch (\MWException $e) {
			$this->outputLine(__METHOD__.": Failed to run report due to: ".$e->getMessage(), time());
			return 1;
		}

		return 0;
	}

	/**
	 * Return cron schedule if applicable.
	 *
	 * @access	public
	 * @return	mixed	False for no schedule or an array of schedule information.
	 */
	public static function getSchedule() {
		return [
			[
				'minutes' => 0,
				'hours' => 6,
				'days' => '*',
				'months' => '*',
				'weekdays' => '*',
				'arguments' => []
			]
		];
	}
}
