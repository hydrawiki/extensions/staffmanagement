<?php
/**
 * Curse Inc.
 * StaffManagement
 * Email Report Card Job
 *
 * @author		rnix
 * @copyright	(c) 2018 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		StaffManagement
 * @link		https://gitlab.com/hydrawiki
 *
**/

namespace StaffManagement\Job;

class EmailReportCardJob extends \SyncService\Job {
	/**
	 * Emails a report card to users in the ReportGroup (Report Managers)
	 *
	 * @access public
	 * @param array $args Unused
	 * @return int Exit value for this thread.
	 */
	public function execute($args = []) {
		try {
			$rc = new \ReportCard();
			$success = $rc->sendReportEmail( \ReportCard::getReportGroupUsers() );
		} catch ( \MWException $e ) {
			$this->outputLine( __METHOD__.": Failed to send report email: ".$e->getMessage(), time() );
			return 1;
		}

		return 0;
	}

	/**
	 * Return a cron schedule for running at 6 a.m. on the first day of each month.
	 *
	 * @access public
	 * @return array An array of schedule information.
	 */
	public static function getSchedule() {
		return [
			[
				'minutes' => 0,
				'hours' => 6,
				'days' => 1,
				'months' => '*',
				'weekdays' => '*',
				'arguments' => []
			]
		];
	}
}
