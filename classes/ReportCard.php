<?php
/**
 * Curse Inc.
 * Staff Management
 * Report Card Template
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class ReportCard {

	/**
	 * constructor
	 */
	public function __construct() {
		$this->DB =	 wfGetDB(DB_MASTER);

	}

	/**
	 * Get data for the Datatable.
	 *
	 * @access	public
	 * @param	boolean	Skip Cache
	 * @return	array
	 */
	public function getReportData($skipCache = false) {
		$redis = RedisCache::getClient('cache');
		$cacheKey = 'sm:reportcard:data';

		if (!$skipCache && $redis !== false) {
			$cache = $redis->get($cacheKey);
			$cache = @json_decode($cache, true);
			if (isset($cache['data']) && isset($cache['debug'])) {
				return $cache;
			}
		}

		$data = [];
		$debug = ['errors' => []];

		$getUsers = $this->DB->select(
			['report_card_users', 'user'],
			['report_card_users.*', 'user.user_name'],
			'',
			__METHOD__,
			[
				'ORDER BY'	=> 'user.user_name ASC'
			],
			[
				'user' => [
					'INNER JOIN', 'user.user_id = report_card_users.user_id'
				]
			]
		);
		$wikiManagers = [];
		while ($row = $getUsers->fetchRow()) {
			$user = User::newFromId($row['user_id']);
			if (!$user || !$user->getId()) {
				$debug['errors']['user_name'] = wfMessage('error_user_not_found')->escaped();
				continue;
			}
			$lookup = \CentralIdLookup::factory();
			$globalId = $lookup->centralIdFromLocalUser($user);
			if (!$globalId) {
				$debug['errors']['global_id'][$user->getId()] = wfMessage('error_user_not_found')->escaped();
			}
			$wikiManagers[$user->getId()]['user'] = $user;
			$wikiManagers[$user->getId()]['global_id'] = $globalId;
		}

		if (count($wikiManagers)) {
			foreach ($wikiManagers as $userId => $info) {
				$user = $info['user'];
				$globalId = $info['global_id'];

				$meta = $this->DB->select(
					['report_card_metadata'],
					['*'],
					['user_id' => $userId],
					__METHOD__
				);

				if (!$meta->result->num_rows) {
					$this->DB->startAtomic(__METHOD__);
					// this user doesn't have a metadata field yet. Better create one.
					$this->DB->insert(
						'report_card_metadata',
						['user_id' => $userId]
				   );
					// lets pull the object back now, with the empty data.
					$meta = $this->DB->select(
						['report_card_metadata'],
						['*'],
						['user_id' => $userId],
						__METHOD__
					);
					$this->DB->endAtomic(__METHOD__);
				}

				// this is probably a dumb way of getting the first row
				foreach ($meta as $m) {
					$meta = $m;
					break;
				}

				$wikiManagers[$userId]['meta'] = $meta;
			}

			$actionStats = [
				'article_edit',
				'article_create',
				'article_delete',
				'article_move',
				'article_merge',
				'article_protect',
				'admin_block_ip',
				'admin_patrol'
			];

			foreach ($wikiManagers as $userId => $info) {
				$wp30 = \Cheevos\Points\PointsDisplay::getWikiPointsForRange($info['global_id'], null, 1);
				$wp90 = \Cheevos\Points\PointsDisplay::getWikiPointsForRange($info['global_id'], null, 3);

				$filters = [
					'limit'		=> 200,
					'offset'	=> 0,
					'user_id'	=> $info['global_id'],
					'global'	=> true
				];

				$months = 1;
				$filters['start_time'] = strtotime(date('Y-m-d', strtotime($months.' month ago')).'T00:00:00+00:00');
				$filters['end_time'] = strtotime(date('Y-m-d', strtotime('yesterday')).'T23:59:59+00:00');
				try {
					$globalStatProgress = \Cheevos\Cheevos::getStatProgress($filters);
				} catch (\Cheevos\CheevosException $e) {
					throw new \ErrorPageError("Cheevos API Error", "Encountered Cheevos API error {$e->getMessage()}\n");
				}

				$actions30 = 0;
				$edits30 = 0;
				foreach ($globalStatProgress as $statProgress) {
					if ($statProgress->getUser_Id() == $info['global_id'] && in_array($statProgress->getStat(), $actionStats)) {
						$actions30 += $statProgress->getCount();
						if ($statProgress->getStat() == 'article_create' || $statProgress->getStat() == 'article_edit') {
							$edits30 += $statProgress->getCount();
						}
					}
				}

				$months = 3;
				$filters['start_time'] = strtotime(date('Y-m-d', strtotime($months.' month ago')).'T00:00:00+00:00');
				$filters['end_time'] = strtotime(date('Y-m-d', strtotime('yesterday')).'T23:59:59+00:00');
				try {
					$globalStatProgress = \Cheevos\Cheevos::getStatProgress($filters);
				} catch (\Cheevos\CheevosException $e) {
					throw new \ErrorPageError("Cheevos API Error", "Encountered Cheevos API error {$e->getMessage()}\n");
				}
				$actions90 = 0;
				$edits90 = 0;
				foreach ($globalStatProgress as $statProgress) {
					if ($statProgress->getUser_Id() == $info['global_id'] && in_array($statProgress->getStat(), $actionStats)) {
						$actions90 += $statProgress->getCount();
						if ($statProgress->getStat() == 'article_create' || $statProgress->getStat() == 'article_edit') {
							$edits90 += $statProgress->getCount();
						}
					}
				}

				$row = [
					"user_id" => $userId,
					"global_id" => $info['global_id'],
					"real_name" => (!empty($info['meta']->real_name) ? $info['meta']->real_name : (!empty($info['user_real_name']) ? $info['user_real_name'] : "Unknown")),
					"username" => $info['user']->getName(),
					"email" => $info['user']->getEmail(),
					"location" => $info['meta']->location,
					"pay_scale" => $info['meta']->pay_scale,
					"wp_30" => $wp30,
					"wp_90" => $wp90,
					"edits_30" => $edits30 ?: 0,
					"edits_90" => $edits90 ?: 0,
					"all_actions_30" => $actions30,
					"all_actions_90" => $actions90 ?: 0,
					"wp_per_day_30" => round(($wp30 / 30), 3) ?: 0,
					"wp_per_day_90" => round(($wp90 / 90), 3) ?: 0,
					"actions_per_day_30" => round(($actions30 / 30), 3) ?: 0,
					"actions_per_day_90" => round(($actions90 / 90), 3) ?: 0,
					"cost_per_wp_30" => '',
					"cost_per_wp_90" => '',
					"cost_per_edit_30" => '',
					"cost_per_edit_90" => '',
					"main_wikis" => $info['meta']->main_wikis,
					"notes" => $info['meta']->notes,
				];

				if ($row['wp_30'] > 0) {
					$row['cost_per_wp_30'] = $row['pay_scale'] / $row['wp_30'];
				} else {
					$row['cost_per_wp_30'] = 0;
				}
				if ($row['wp_90'] > 0) {
					$row['cost_per_wp_90'] = ($row['pay_scale'] * 3) / $row['wp_90'];
				} else {
					$row['cost_per_wp_90'] = 0;
				}
				if ($row['edits_30'] > 0) {
					$row['cost_per_edit_30'] = $row['pay_scale'] / $row['edits_30'];
				} else {
					$row['cost_per_edit_30'] = 0;
				}
				if ($row['edits_90'] > 0) {
					$row['cost_per_edit_90'] = ($row['pay_scale'] * 3) / $row['edits_90'];
				} else {
					$row['cost_per_edit_90'] = 0;
				}

				$row['cost_per_wp_30'] = "$".number_format($row['cost_per_wp_30'], 2, '.', ',');
				$row['cost_per_wp_90'] = "$".number_format($row['cost_per_wp_90'], 2, '.', ',');
				$row['cost_per_edit_30'] = "$".number_format($row['cost_per_edit_30'], 2, '.', ',');
				$row['cost_per_edit_90'] = "$".number_format($row['cost_per_edit_90'], 2, '.', ',');

				// never return null. return empty.
				foreach (array_keys($row) as $key) {
					if ($row[$key] == null) {
						$row[$key] = "";
					}
				}

				// dollar signs on pay scale.
				if (!is_numeric($row['pay_scale'])) {
					$row['pay_scale'] = 0;
				}
				$row['pay_scale'] = "$".number_format($row['pay_scale']);

				$data[] = $row;
			}
		} else {
			$debug[] = "No Wiki Managers found!";
		}

		$return = ['data' => $data, 'debug' => $debug];
		if ($redis !== false) {
			$redis->setEx($cacheKey, 86400, json_encode($return));
		}

		return ['data' => $data, 'debug' => $debug];
	}

	/**
	 * save new user
	 * @return true
	 */
	public function saveNewUser($userName) {
		$userName = \User::getCanonicalName($userName);

		$result = $this->DB->select(
			'user',
			['*'],
			['user_name' => $userName],
			__METHOD__
		);
		$user = $result->fetchRow();

		if (!$user) {
			$title = "error";
			$result = "User not found";

		} else {
			$user_id = $user['user_id'];
			try {
				$this->DB->startAtomic(__METHOD__);
				$this->DB->insert(
					'report_card_users',
					['user_id' => $user_id]
				);
				$this->DB->endAtomic(__METHOD__);
				$title = "message";
				$result = "User added";
			} catch (Exception $e) {
				$title = "error";
				$result = "Database error when adding user. ".$e->getMessage()."";
			}

		}

		return [$title => $result];

	}

	/**
	 * delete user
	 * @return true
	 */
	public function deleteUser($user_id) {
		$this->DB->delete(
			'report_card_users',
			['user_id' => $user_id]
		);
		return true;
	}

	/**
	 * Clear meta data for a user
	 * @return true
	 */
	public function deleteMeta($user_id) {
		$this->DB->delete(
			'report_card_metadata',
			['user_id' => $user_id]
		);
		true;
	}

	/**
	 * save metadata
	 * @return true
	 */
	public function saveMeta($user_id,$field,$value) {

		// strip extra currency stuff from pay_scale to store as an int.
		if ($field == "pay_scale") {
			$value = (int)preg_replace("/([^0-9\\.])/i", "", $value);
		}

		$this->DB->update(
			'report_card_metadata',
			[$field => $value],
			['user_id' => $user_id]
		);
		return true;
	}

	/**
	 * Send report email with csv attachment
	 *
	 * @access	private
	 * @param	array	User objects to email.
	 * @return	boolean If the email sending was successful.
	 */
	public function sendReportEmail($users) {
		global $wgNoReplyAddress;

		$data = $this->getReportData();

		foreach ($data['data'] as $row) {
			if (!isset($csv)) {
				$csv = implode(",", array_keys($row));
			}
			$vals = array_values($row);
			foreach ($vals as $i => $v) {
				$vals[$i] = str_replace(",", "", $v); // commas break csv.
			}
			$csv .= "\r\n".implode(",", $vals);
		}

		$csv = chunk_split(base64_encode($csv));
		$separator = md5(time());

		$emailSubject	= wfMessage('report_card')->escaped();
		$emailBody		= "Your report is attached.";

		$address = [];
		foreach ($users as $user) {
			$userEmail = $user->getEmail();
			if ($user->getId() && !empty($userEmail)) {
				$address[] = new MailAddress($userEmail, $user->getName());
			}
		}

		// message
		$mimeBody .= "--".$separator."\r\n";
		$mimeBody .= "Content-Type: text/plain; charset=\"iso-8859-1\""."\r\n";
		$mimeBody .= "Content-Transfer-Encoding: quoted-printable"."\r\n";
		$mimeBody .= "Your report is attached.\r\n\r\n";

		// attachment
		$mimeBody .= "--".$separator."\r\n";
		$mimeBody .= "Content-Type: application/octet-stream; name=\"report.csv\""."\r\n";
		$mimeBody .= "Content-Transfer-Encoding: base64"."\r\n";
		$mimeBody .= "Content-Disposition: attachment"."\r\n";
		$mimeBody .= $csv."\r\n\r\n";

		// ending separator
		$mimeBody .= "--".$separator."\r\n";

		$reportID = self::$reportID++;
		self::$reportEmails[$reportID] = [
			'body'  => $mimeBody,
			'ctype' => "multipart/mixed; boundary=\"".$separator."\""
		];

		$email = new UserMailer();
		$status = $email->send(
			$address,
			new MailAddress( $wgNoReplyAddress ),
			$emailSubject,
			[
				'text' => $emailBody,
				'html' => $emailBody,
			],
			[
				'headers' => [
					'X-Report-Card-ID' => $reportID
				]
			]
		);

		return $status->ok;
	}

	/**
	 * Set email headers and body for attaching a report card to an emiil
	 *
	 * @param	array &$headers Email header array
	 * @param	string &$body Email body
	 * @return	bool True if the report ID is valid
	 */
	public static function insertEmailAttachment( $reportID, &$headers, &$body ) {
		if ( !array_key_exists( $reportID, self::$reportEmails ) ) {
			return false;
		}

		$email = self::$reportEmails[$reportID];
		$body = $email['body'];
		$headers['Content-Type'] = $email['ctype'];
		return true;
	}

	/**
	 * ID used to track ReportCards for a mail attachment hook
	 */
	private static $reportID = 0;

	/**
	 * Array from id to report card email
	 */
	private static $reportEmails = [];

	/**
	 * Get list of users in the reporting group (report_manager)
	 *
	 * @access	public
	 * @return	array List of Users.
	 */
	public static function getReportGroupUsers() {
		global $wgSMReportGroup;

		$db = wfGetDB( DB_MASTER );
		$result = $db->select(
			[ 'user_groups' ],
			[ 'ug_user' ],
			[
				"user_groups.ug_group" => $wgSMReportGroup
			],
			__METHOD__
		);

		$users = [];

		while ( $row = $result->fetchObject() ) {
			$users[] = User::newFromId( $row->ug_user );
		}

		return $users;
	}
}
