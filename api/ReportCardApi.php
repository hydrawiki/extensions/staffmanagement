<?php
/**
 * Curse Inc.
 * Staff Management
 * Report Card API
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class ReportCardApi extends ApiBase {

    /**
     * Execute the API
     * @return true
     */
	public function execute() {
        global $wgSMReportGroup;

        if (!$this->getUser()->isAllowed('view_report_cards')) {
            $this->dieWithError('You do not have access too this', 'acess_denied', null, 403);
        }

        $rc = new \ReportCard();

        // this is thrown by DataTables sometimes. Avoid errors.
        $this->getMain()->getVal( '_' );

        $path = $this->getMain()->getVal( 'x' );
        switch ($path) {
            case "api":
                $data = $rc->getReportData(true);
            break;
            case "api/save":
                $field = $this->getMain()->getVal('field');
                $user_id = $this->getMain()->getVal('user_id');
                $value = $this->getMain()->getVal('value');
                $rc->saveMeta($user_id, $field, $value);
            break;
            case "api/savenew":
                $user_name = $this->getMain()->getVal('user_name');
                $data = $rc->saveNewUser($user_name);
            break;
            case "api/deleteuser":
                $user_id = $this->getMain()->getVal('user_id');
                $rc->deleteUser($user_id);
            break;
            case "api/deletemeta":
                $user_id = $this->getMain()->getVal('user_id');
                $rc->deleteMeta($user_id);
            break;
        }

        if ($data) {
            // if data is an array containing data, there may also be debug info to view.
            if (isset($data['data'])) {
                $this->getResult()->addValue( null, 'data', $data['data'] );
                if (isset($data['debug'])) {
                    $this->getResult()->addValue( null, 'debug', $data['debug'] );
                }
            } else {
                $this->getResult()->addValue( null, 'data', $data );
            }
        }

        return true;
	}
}
