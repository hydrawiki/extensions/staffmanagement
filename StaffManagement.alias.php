<?php
/**
 * Curse Inc.
 * Staff Management
 * Staff Management Aliases
 *
 * @author 		Collin Klopfenstein
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

$specialPageAliases = array();

/** English (English) */
$specialPageAliases['en'] = [
	'GlobalStats' => ['GlobalStats'],
	'ReportCard' => ['ReportCard'],
	'UserStats' => ['UserStats'],
	'WikiStats' => ['WikiStats'],
	'WikiUserStats' => ['WikiUserStats']
];
?>