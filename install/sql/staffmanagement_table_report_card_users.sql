CREATE TABLE /*_*/report_card_users (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(10) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `user_id` (`user_id`)
) /*$wgDBTableOptions*/