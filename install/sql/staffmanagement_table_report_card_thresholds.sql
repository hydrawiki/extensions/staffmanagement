CREATE TABLE/*_*/report_card_thresholds (
  `tid` int(14) NOT NULL AUTO_INCREMENT,
  `points` int(14) NOT NULL DEFAULT '0',
  `score` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tid`),
  KEY `points` (`points`)
) /*$wgDBTableOptions*/ 