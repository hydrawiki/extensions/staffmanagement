CREATE TABLE /*_*/report_card_metadata (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `pay_scale` int(11) DEFAULT NULL,
  `real_name` text,
  `email` text,
  `location` text,
  `main_wikis` text,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) /*$wgDBTableOptions*/ 