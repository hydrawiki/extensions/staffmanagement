<?php
/**
 * Curse Inc.
 * Staff Management
 * Global Stats Skin
 *
 * @author		Collin Klopfenstein
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class TemplateWikiUserStats {
	/**
	 * Return page URLs for statistics.
	 *
	 * @access	protected
	 * @param	string	Site Key
	 * @return	array	String URLs
	 */
	protected function getUrls($siteKey) {
		$page				= Title::newFromText('Special:WikiUserStats');
		$detailPage			= Title::newFromText('Special:UserStats');
		$wikiUserStatsUrl	= $page->getFullURL(['site_key' => $siteKey]);
		$userStatsUrl		= $detailPage->getFullURL();

		return [$wikiUserStatsUrl, $userStatsUrl];
	}

	/**
	 * Wiki User Stats Template
	 *
	 * @access	public
	 * @param	array	Array of \Cheevos\CheevosStatProgress objects.
	 * @param	string	Site Key of the Wiki
	 * @param	array	Pagination
     * @param	string	[Optional] Sort Direction
     * @param	string	[Optional] Sort Field
	 * @return	string	Built HTML
	 */
	public function wikiUserStats($statProgress, $siteKey, $pagination, $sortDir = 'asc', $sortField = 'actions') {
		global $wgOut, $wgUser, $wgRequest;

		list($wikiUserStatsUrl, $userStatsUrl) = $this->getURLs($siteKey);

		$oppositeSort = 'desc';
		if ($sortDir == 'desc') {
			$oppositeSort = 'asc';
		}

        $html = "
	<div>{$pagination}</div>
	<table id='userstats'>
		<thead>
			<tr>
				<th rowspan=\"2\">".wfMessage('user_name')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('points')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('edits')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('deletes')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('patrols')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('blocks')->escaped()."</th>
			</tr>
			<tr>";
		foreach (StaffManagement::$trackedStats as $stat) {
			$html .= "
				<th><a ".($sortField == $stat ? " class=\"active ".$sortDir."\"" : null)." href=\"{$wikiUserStatsUrl}".(strpos($wikiUserStatsUrl, '?') === false ? '?' : '&')."dir=".($sortField == $stat && $sortDir ? $oppositeSort : 'desc')."&field={$stat}\">".wfMessage('total')->escaped()."</a></th>
				<th>30</th>
				<th>60</th>
				<th>90</th>";
		}
		$html .= "
			</tr>
		</thead>
		<tbody>
		";
		if (count($statProgress)) {
			foreach ($statProgress as $globalId => $info) {
				$user = $info['user'];
				$html .= "
				<tr>
					<td><a href='{$userStatsUrl}?globalId={$globalId}'>".$user->getName()."</a></td>";
				foreach (StaffManagement::$trackedStats as $stat) {
					foreach (range(0, 3) as $monthsAgo) {
						$html .= "<td>".(isset($info[$monthsAgo][$stat]) ? $info[$monthsAgo][$stat]->getCount() : 0)."</td>";
					}
				}
				$html .= "
				</tr>";
            }
        } else {
            $html .= "
			<tr>
				<td colspan='21'>".wfMessage('no_users_found')->escaped()."</td>
			</tr>
			";
        }
        $html .= "
		</tbody>
	</table>";

        $html .= $pagination;

        return $html;
    }

	/**
	 * Generates pagination template.
	 *
	 * @access	public
	 * @param	array	Array of pagination information.
	 * @param	string	Site Key
	 * @return	string	Built HTML
	 */
	public function paginationTemplate($pagination, $siteKey) {
		if (count($pagination['pages'])) {
$html .= <<<HTML
	<ul class='pagination'>
HTML;
			if ($pagination['stats']) {
				$html .= "<li class='pagination_stats'>Page {$pagination['stats']['current_page']} of {$pagination['stats']['pages']}</li>";
			}

			if (count($pagination['pages']) > 1) {
				if ($pagination['first']) {
					$html .= "<li><a href='?site_key={$siteKey}&st={$pagination['first']['st']}'>&laquo;</a></li>";
				}
				foreach ($pagination['pages'] as $page => $info) {
					if ($page > 0) {
						$html .= "<li".($info['selected'] ? " class='selected'" : null)."><a href='?site_key={$siteKey}&st={$info['st']}'>{$page}</a></li>";
					}
				}
				if ($pagination['last']) {
					$html .= "<li><a href='?site_key={$siteKey}&st={$pagination['last']['st']}'>&raquo;</a></li>";
				}
			}
$html .= "
	</ul>";

		return $html;
		}
	}
}
