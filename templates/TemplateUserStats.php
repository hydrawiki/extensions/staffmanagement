<?php
/**
 * Curse Inc.
 * Staff Management
 * User Stats Skin
 *
 * @author		Collin Klopfenstein
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class TemplateUserStats {
    /**
     * Output HTML
     *
     * @var		string
     */
    private $HMTL;

    /**
     * User Wiki Stats Page
     *
     * @access	public
     * @param	array	Array of user wiki statistics.
     * @param	array	Statistics to Display
     * @param	array	Months to Display
     * @param	array	Sorted Keys
     * @param	string	[Optional] Sort Direction
     * @param	string	[Optional] Sort Field
     * @param	string	[Optional] Sort Time
     * @return	string	Built HTML
     */
    public function userWikiStatsPage($userWikiStats, $stats, $months, $sort, $sortDir = 'asc', $sortField = 'actions', $sortTime = 'total') {
		$statsPage	= Title::newFromText('Special:UserStats');
		$statsURL	= $statsPage->getFullURL();

		$wikiUserStatsPage	= Title::newFromText('Special:WikiUserStats');
		$wikiUserStatsURL	= $wikiUserStatsPage->getFullURL();

		$oppositeSort = 'desc';
		if ($sortDir == 'desc') {
			$oppositeSort = 'asc';
		}

		$lookup = \CentralIdLookup::factory();
		$globalId = $lookup->centralIdFromLocalUser($userWikiStats['user']);
        $html = "
	<table id='userstats'>
		<thead>
			<tr>
				<th rowspan='2'>".wfMessage('wiki')->escaped()."</th>";
		foreach ($stats as $stat) {
			$html .= "
				<th colspan='4'>".wfMessage('sm_'.$stat)->escaped()."</th>";
		}
			$html .= "
			</tr>
			<tr>";
		foreach ($stats as $stat) {
			$html .= "
				<th><a ".(($sortField == $stat && $sortTime == 'total') || (!$sortField && !$sortTime) ? " class=\"active ".($sortDir ? $sortDir : 'desc')."\"" : null)." href=\"{$statsURL}?globalId=".$globalId."&dir=".($sortField == $stat && $sortTime == 'total' && $sortDir ? $oppositeSort : 'desc')."&field={$stat}&time=total\">".wfMessage('total')->escaped()."</a></th>";
			foreach ($months as $month) {
				$html .= "
				<th><a ".($sortField == $stat && $sortTime == $month ? " class=\"active {$sortDir}\"" : null)." href=\"{$statsURL}?globalId=".$globalId."&dir=".($sortField == $stat && $sortTime == $month && $sortDir ? $oppositeSort : 'desc')."&field={$stat}&time={$month}\">$month</a></th>";
			}
		}
		$html .= "
			</tr>
		</thead>
		<tbody>";
        if (count($userWikiStats['wikis'])) {
            foreach ($sort as $siteKey => $scale) {
				$wiki = $userWikiStats['wikis'][$siteKey];
                $html .= "
			<tr>
				<td><a href='{$wikiUserStatsURL}?site_key={$siteKey}'>".(array_key_exists('site', $wiki) && $wiki['site'] !== false ? $wiki['site']->getDomains()->getDomain() : '&nbsp;')."</a></td>";
				foreach ($wiki as $stat => $month) {
					if ($stat === 'site') {
						continue;
					}
					$html .= "
				<td>".(isset($wiki[$stat]['total']) ? $wiki[$stat]['total'] : '-')."</td>";
					foreach ($months as $month) {
						$html .= "
				<td>".(isset($wiki[$stat][$month]) ? $wiki[$stat][$month] : '-')."</td>";
					}
				}
				$html .="
			</tr>";
            }
        } else {
            $html .= "
			<tr>
				<td colspan='21' align='center'>".wfMessage('no_stats_found')->escaped()."</td>
			</tr>
			";
        }
        $html .= "
		</tbody>
	</table>";

        return $html;
    }
}
