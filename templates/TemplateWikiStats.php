<?php
/**
 * Curse Inc.
 * Staff Management
 * Wiki Stats Skin
 *
 * @author		Collin Klopfenstein, Tim Aldridge
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class TemplateWikiStats {
    /**
     * Output HTML
     *
     * @var		string
     */
    private $HMTL;

    /**
     * Wiki Stats Template
     *
     * @access	public
     * @param	array	Array of wiki advertisement information
     * @param	array	Pagination
     * @param	string	Sort Direction
     * @param	string	Sort Field
     * @param	string	Search Term
     * @return	string	Built HTML
     */
    public function wikiStats($wikis, $pagination, $sortKey, $sortDir, $searchTerm) {
		$statsPage		= Title::newFromText('Special:WikiStats');
		$statsURL		= $statsPage->getFullURL();
		$userStatsPage	= Title::newFromText('Special:WikiUserStats');
		$userStatsURL	= $userStatsPage->getFullURL();

		$oppositeSort = 'desc';
		if ($sortDir == 'desc') {
			$oppositeSort = 'asc';
		}

        $html = "
	<div>{$pagination}</div>
	<div class='button_bar'>
		<div class='buttons_left'>
			<form method='get' action='{$statsURL}'>
				<fieldset>
					<input type='hidden' name='section' value='list' />
					<input type='hidden' name='do' value='search' />
					<input type='text' name='list_search' value='".htmlentities($searchTerm, ENT_QUOTES)."' class='search_field' />
					<input type='submit' value='".wfMessage('list_search')->escaped()."' class='mw-ui-button mw-ui-progressive'/>
					<a href='{$statsURL}?do=resetSearch' class='mw-ui-button mw-ui-destructive'>".wfMessage('list_reset')->escaped()."</a>
				</fieldset>
			</form>
		</div>
	</div>
	<table id='userstats'>
		<thead>
			<tr>
				<th><a ".($sortKey == 'wiki_name' || !$sortKey ? " class=\"active ".($sortDir ? $sortDir : 'asc')."\"" : null)." href=\"{$statsURL}?sort_dir=".($sortKey == 'wiki_name' && $sortDir ? $oppositeSort : 'desc')."&sort=wiki_name\">".wfMessage('wiki_name')->escaped()."</th>
				<th><a ".($sortKey == 'domain' ? " class=\"active ".$sortDir."\"" : null)." href=\"{$statsURL}?sort_dir=".($sortKey == 'domain' && $sortDir ? $oppositeSort : 'desc')."&sort=domain\">".wfMessage('wiki_domain')->escaped()."</th>
				<th>".wfMessage('wiki_category')->escaped()."</th>
			</tr>
		</thead>
		<tbody>
		";
        if (is_array($wikis) && count($wikis)) {
            foreach ($wikis as $siteKey => $wiki) {
                 $html .= "
				<tr>
					<td><a href=\"{$userStatsURL}?site_key={$wiki->getSiteKey()}\">{$wiki->getName()} (".strtoupper($wiki->getLanguage()).")</a></td>
					<td align=\"center\">{$wiki->getDomains()->getDomain()}</td>
					<td align=\"center\">{$wiki->getCategory()}</td>
				</tr>";
            }
        } else {
            $html .= "
			<tr>
				<td colspan='21' align=\"center\">".wfMessage('no_wikis_found')->escaped()."</td>
			</tr>
			";
        }
        $html .= <<<HTML
		</tbody>
	</table>
HTML;

        $html .= $pagination;

        return $html;
    }
}
