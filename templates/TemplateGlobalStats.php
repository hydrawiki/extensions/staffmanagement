<?php
/**
 * Curse Inc.
 * Staff Management
 * Global Stats Skin
 *
 * @author		Collin Klopfenstein
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class TemplateGlobalStats {
	/**
	 * Return page URLs for statistics.
	 *
	 * @access	protected
	 * @return	array	String URLs
	 */
	protected function getUrls() {
		$page			= Title::newFromText('Special:GlobalStats');
		$detailPage		= Title::newFromText('Special:UserStats');
		$globalStatsUrl	= $page->getFullURL();
		$userStatsUrl	= $detailPage->getFullURL();

		return [$globalStatsUrl, $userStatsUrl];
	}

	/**
	 * Global Stats Template
	 *
	 * @access	public
	 * @param	array	Array of \Cheevos\CheevosStatProgress objects.
	 * @param	array	Pagination
     * @param	string	[Optional] Sort Direction
     * @param	string	[Optional] Sort Field
	 * @param	string	Search Term
	 * @return	string	Built HTML
	 */
	public function globalStats($statProgress, $pagination, $sortDir = 'asc', $sortField = 'actions', $searchTerm) {
		list($globalStatsUrl, $userStatsUrl) = $this->getUrls();

		$oppositeSort = 'desc';
		if ($sortDir == 'desc') {
			$oppositeSort = 'asc';
		}

        $html = "
	<div>{$pagination}</div>
	<div class='button_bar'>
		<div class='buttons_left'>
			<form method='get' action='{$globalStatsUrl}'>
				<fieldset>
					<input type='hidden' name='section' value='list'/>
					<input type='hidden' name='do' value='search'/>
					<input type='text' name='list_search' value='".htmlentities($searchTerm, ENT_QUOTES)."' class='search_field' placeholder='".wfMessage('search')->escaped()."'/>
					<input type='submit' value='".wfMessage('list_search')->escaped()."' class='mw-ui-button mw-ui-progressive'/>
					<a href='{$globalStatsUrl}?do=resetSearch' class='mw-ui-button mw-ui-destructive'>".wfMessage('list_reset')->escaped()."</a>
				</fieldset>
			</form>
		</div>
	</div>
	<table id='userstats'>
		<thead>
			<tr>
				<th rowspan=\"2\">".wfMessage('user_name')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('points')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('edits')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('deletes')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('patrols')->escaped()."</th>
				<th colspan=\"4\">".wfMessage('blocks')->escaped()."</th>
			</tr>
			<tr>";
		foreach (StaffManagement::$trackedStats as $stat) {
			$html .= "
				<th><a ".($sortField == $stat ? " class=\"active ".$sortDir."\"" : null)." href=\"{$globalStatsUrl}".(strpos($globalStatsUrl, '?') === false ? '?' : '&')."dir=".($sortField == $stat && $sortDir ? $oppositeSort : 'desc')."&field={$stat}\">".wfMessage('total')->escaped()."</a></th>
				<th>30</th>
				<th>60</th>
				<th>90</th>";
		}
		$html .= "
			</tr>
		</thead>
		<tbody>
		";
		if (count($statProgress)) {
			foreach ($statProgress as $globalId => $info) {
				$user = $info['user'];
				$html .= "
				<tr>
					<td><a href='{$userStatsUrl}?globalId={$globalId}'>".$user->getName()."</a></td>";
				foreach (StaffManagement::$trackedStats as $stat) {
					foreach (range(0, 3) as $monthsAgo) {
						$html .= "<td>".(isset($info[$monthsAgo][$stat]) ? $info[$monthsAgo][$stat]->getCount() : 0)."</td>";
					}
				}
				$html .= "
				</tr>";
            }
        } else {
            $html .= "
			<tr>
				<td colspan='21'>".wfMessage('no_users_found')->escaped()."</td>
			</tr>
			";
        }
        $html .= <<<HTML
		</tbody>
	</table>
HTML;

        $html .= $pagination;

        return $html;
    }
}
