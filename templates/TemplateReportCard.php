<?php
/**
 * Curse Inc.
 * Staff Management
 * Report Card Template
 *
 * @author		Alex Smith
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Staff Management
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class TemplateReportCard {
	/**
	 * Report Card Form
	 *
	 * @access public
	 * @param array [Optional] User information
	 * @param array [Optional] Wiki Managers to show report card table for.
	 * @return string Built HTML
	 */
	public function reportCardForm() {
		$html = "
			 <table id=\"reportcard\" class=\"cell-border compact hover order-column stripe\">
				  <thead>
						<tr>
							 <th data-id=\"user_id\" data-group=\"hide\" data-editable=\"false\">User ID</th>
							 <th data-id=\"real_name\" data-group=\"meta\" data-editable=\"true\">Real Name</th>
							 <th data-id=\"username\" data-group=\"meta\" data-editable=\"false\">Username</th>
							 <th data-id=\"email\" data-group=\"meta\" data-editable=\"true\">Email</th>
							 <th data-id=\"location\" data-group=\"meta\" data-editable=\"true\">Location</th>
							 <th data-id=\"pay_scale\" data-group=\"meta\" data-editable=\"true\">Payscale</th>

							 <th data-id=\"wp_30\" data-group=\"30\" data-editable=\"false\">Wiki Points <small>".wfMessage('reportcard-passed', 1)->escaped()."</small></th>
							 <th data-id=\"edits_30\" data-group=\"30\" data-editable=\"false\">Edits <small>".wfMessage('reportcard-passed', 1)->escaped()."</small></th>
							 <th data-id=\"all_actions_30\" data-group=\"30\" data-editable=\"false\">All Actions <small>".wfMessage('reportcard-passed', 1)->escaped()."</small></th>
							 <th data-id=\"wp_per_day_30\" data-group=\"30\" data-editable=\"false\">WP/day <small>".wfMessage('reportcard-passed', 1)->escaped()."</small></th>
							 <th data-id=\"actions_per_day_30\" data-group=\"30\" data-editable=\"false\">Action/day <small>".wfMessage('reportcard-passed', 1)->escaped()."</small></th>
							 <th data-id=\"cost_per_wp_30\" data-group=\"30\" data-editable=\"false\">Cost per WP <small>".wfMessage('reportcard-passed', 1)->escaped()."</small></th>
							 <th data-id=\"cost_per_edit_30\" data-group=\"30\" data-editable=\"false\">Cost per Edit <small>".wfMessage('reportcard-passed', 1)->escaped()."</small></th>

							 <th data-id=\"wp_90\" data-group=\"90\" data-editable=\"false\">Wiki Points <small>".wfMessage('reportcard-passed', 3)->escaped()."</small></th>
							 <th data-id=\"edits_90\" data-group=\"90\" data-editable=\"false\">Edits <small>".wfMessage('reportcard-passed', 3)->escaped()."</small></th>
							 <th data-id=\"all_actions_90\" data-group=\"90\" data-editable=\"false\">All Actions <small>".wfMessage('reportcard-passed', 3)->escaped()."</small></th>
							 <th data-id=\"wp_per_day_90\" data-group=\"90\" data-editable=\"false\">WP/day <small>".wfMessage('reportcard-passed', 3)->escaped()."</small></th>
							 <th data-id=\"actions_per_day_90\" data-group=\"90\" data-editable=\"false\">Action/day <small>".wfMessage('reportcard-passed', 3)->escaped()."</small></th>
							 <th data-id=\"cost_per_wp_90\" data-group=\"90\" data-editable=\"false\">Cost per WP <small>".wfMessage('reportcard-passed', 3)->escaped()."</small></th>
							 <th data-id=\"cost_per_edit_90\" data-group=\"90\" data-editable=\"false\">Cost per Edit <small>".wfMessage('reportcard-passed', 3)->escaped()."</small></th>

							 <th data-id=\"main_wikis\" data-group=\"meta\" data-editable=\"true\">Main Wikis</th>
							 <th data-id=\"notes\" data-group=\"meta\" data-editable=\"true\">Notes</th>
                             <th data-id=\"actions\" data-group=\"controls\" data-editable=\"false\"></th>
						</tr>
				  <thead>
				  <tbody>
				  </tbody>
			 </table>";


		return $html;
	}

	/**
	 * Report Card Form
	 *
	 * @access public
	 * @param array $thresholds [Optional] Array of existing thresholds.
	 * @param array $errors [Optional] Key name => Error of errors
	 * @param boolean $success [Optional] If the save was successful.
	 * @return string Built HTML
	 */
	public function thresholdsForm($thresholds = [], $errors = [], $success = false) {
		global $wgScriptPath;

		if ($success) {
			$html .= "
		<div class='successbox'>
			<strong><p>".wfMessage('thresholds_updated')->escaped()."</p></strong>
		</div>";
		}

		$html .= "
		<form id='thresholds_form' method='post' action='?do=save'>
			<fieldset>
				<legend>".wfMessage('modify_thresholds')->escaped()." <input class='add_threshold' name='add_threshold' type='button' value='".wfMessage('add_threshold')->escaped()."'/></legend>
				<div class='threshold_head'><span>".wfMessage('points')->escaped()."</span><span>".wfMessage('score')->escaped()."</span></div>
				".($errors['threshold'] ? '<span class="error">'.$errors['threshold'].'</span>' : '');
		if (is_array($thresholds) and count($thresholds)) {
			foreach ($thresholds as $points => $score) {
				$points = htmlentities($points, ENT_QUOTES);
				$score = htmlentities($score, ENT_QUOTES);
				$html .= "<div class='threshold'><input name='thresholdPoints[]' value='{$points}' type='text'/><input name='thresholdScores[]' value='{$score}' type='text'/><img src='".wfExpandUrl("{$wgScriptPath}/extensions/StaffManagement/images/delete.png")."' class='delete_threshold'></div>";
			}
		}

		$html .= "
			</fieldset>
			<fieldset>
				".wfMessage('thresholds_explanation')->escaped()."<br/>
				<input id='thresholds_submit' type='submit' value='".wfMessage('save_thresholds')->escaped()."'/>
			</fieldset>
		</form>
		";

		return $html;
	}
}
