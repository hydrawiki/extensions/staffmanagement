<?php
/**
 * StaffManagement
 * Email ReportCard
 *
 * @license     GPL
 * @package     Email Test
 *
 **/
require_once(__DIR__."/../../../maintenance/Maintenance.php");

class EmailReportCard extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Send the current Report Card report to the report_manager group.";
	}

	/**
	 * Send a test email.
	 *
	 * @access	public
	 * @return	void
	 */
	public function execute() {
		$rc = new ReportCard();
		$success = $rc->sendReportEmail( ReportCard::getReportGroupUsers() );

		if ($success) {
			echo "\n\nThe report email was sent.\n\n";
		} else {
			echo "\n\nThe report email failed to send.\n";
		}
	}
}

$maintClass = "EmailReportCard";
require_once(RUN_MAINTENANCE_IF_MAIN);
